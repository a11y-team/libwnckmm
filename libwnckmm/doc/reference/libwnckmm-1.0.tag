<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="group">
    <name>wnckmmEnums</name>
    <title>wnckmm Enums and Flags</title>
    <filename>a00021.html</filename>
    <member kind="enumeration">
      <type></type>
      <name>PagerDisplayMode</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga62025e8ad5baa49eb952912ca4c1a4bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PAGER_DISPLAY_NAME</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga62025e8ad5baa49eb952912ca4c1a4bfae7d1779200567f51215a5f6e89430c78</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PAGER_DISPLAY_CONTENT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga62025e8ad5baa49eb952912ca4c1a4bfaa1301311f7be5541137d611fc41cd5a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowType</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gafc09fd14bddb47633ce0f0c873c03a5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_NORMAL</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa7473ddd8001f6398118b9ad45f858637</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_DESKTOP</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aadcc603879bfdae2bdb7a9708376877eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_DOCK</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa667582eaa7887513eb1f5a05b9ae6484</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_DIALOG</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa1a43618225a733c11533e60f49061ac3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_TOOLBAR</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aab81cd0d9b4a5058f373080161e81d399</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_MENU</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aaffdf6568ca625adaafaf243425e02147</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_UTILITY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa96efe27eec0befaec5bacfdc74164ce4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_SPLASHSCREEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa603924300cb68522bdd9ff92a9ad7fc1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowState</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga22305f517749f485e0172d74d35ab9f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_MINIMIZED</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a5e3643b2e424f3e6d9c4cacec13893e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_MAXIMIZED_HORIZONTALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ac39344f28ea43e719e095dcf55781018</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_MAXIMIZED_VERTICALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a3fba6fc435098c81784068658b5ec933</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_SHADED</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a4ff7aee054d5c745d4c6f39c31d7b8a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_SKIP_PAGER</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ab11ebc50004373097b110c747a98d6a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_SKIP_TASKLIST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a7ce7becde85ed316c4382da5d50438ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_STICKY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a365dee25cf9259f28b0cbb90fd2dda4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_HIDDEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a0efe2534808d0f7360981c8caf2db03b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_FULLSCREEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0af45ef5b2cbc076b80ecc5562cf01fb0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_DEMANDS_ATTENTION</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0af8cd40e582a8dfd50d95486d10d0cc5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_URGENT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a00342aafcf568af56f1179045e4c57ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_ABOVE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ae5208f34e3e8da19f23f3e8ed5d7daaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_BELOW</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ae094e96615a1d2f648b8dbb375ca2c16</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowActions</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga4554e5bb7dcbe177c55d45c106e5ae3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MOVE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa354275bfada4b85f73e3cdeb209102bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_RESIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fac0602212934a0f175881eac5130884d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_SHADE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa3c220b6d5b93188471f736c8f2879daf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_STICK</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fae2c11c98576901ccbd9d01d913530a7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MAXIMIZE_HORIZONTALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa896ab891a6d22ad98ff42c178ee6f67d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MAXIMIZE_VERTICALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fad870b636e87aea30194797fbd39ade3e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_CHANGE_WORKSPACE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fad54c6ba6be26c3ee09fbce56a88135db</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_CLOSE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa1a39b09118e25084fc1cf5c7905acb8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMAXIMIZE_HORIZONTALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3facff11c69e23432e0b3fc849eafb4c0f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMAXIMIZE_VERTICALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fae4e2090d7ef260ad50fb3d562f1d5735</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNSHADE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faed6c72022132c6912fa110f354d2a3a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNSTICK</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa38a6ec6f30e16a78a9562348ade947b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MINIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faae09d8b9a147219664a0506b98f30cc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMINIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faa246a84b101a90eef672fd9ba6654a2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MAXIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa3b2142dd6f0bb4fe41d2bb4ddeda5ed7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMAXIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faf119a507e8da0a135542c8725d5b2e8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_FULLSCREEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa3badbbfb5a4ce4c03b3569312baf866e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_ABOVE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faed40a6f35230a45ee4564eac7b05d051</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_BELOW</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fad38e2327f985f43271b3def9c755bb12</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowGravity</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga1e0e1c715ff933cda7ad971d905fca7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_CURRENT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa2c32445e0fab2887df4379383ff8ef8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_NORTHWEST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa8b8262cc0f0c74aacf41b8204263db26</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_NORTH</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa3450459c7598dd6b6809d06849c33b4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_NORTHEAST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa43cd181291a7e83c19ddbbfd0d99d74a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_WEST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aaa51fac9afcd31a62d1af1c3d1af804c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_CENTER</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa5841b107054c9a0fd914e22f9d3d1ff7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_EAST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa3d8077f4ed7b6a5e34e49bfbf426042a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_SOUTHWEST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa27b1b3322463590f734efd786c755239</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_SOUTH</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa55fb1c778605892f6a4261e7af5dfbe4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_SOUTHEAST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aaccccd8da3cd9e7d4f0fc90d9eb90cba9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_STATIC</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aad0682221d70034a2bcbc8e49cba4f412</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowMoveResizeMask</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gab6325f24c6a98ac725413e19747897e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_X</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0a4bd34c04608c0162bbd569010807cca7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_Y</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0a9c5ee5e8617f53613b9abc4aa549d9aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_WIDTH</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0ae347219e8602dc92e40668a88366929e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_HEIGHT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0a4d6b7e6fae889f26240536d0c4f4682d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>MotionDirection</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gabf9331c1c4adf47bb2541f987c060934</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_UP</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934ac2d3296d40074d4787e7af10706ea830</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_DOWN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934a7f6600e4d4189fb51b41e4de14af1259</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_LEFT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934a56a0ad356bc614ec646768ed7dbbb021</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_RIGHT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934a1a5cfc200889357d5f51b1d16e38ddf9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator|</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gac18aa71cc6301d547df48651d2282002</anchor>
      <arglist>(WindowState lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator&amp;</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga30f8fbe65446e9e73cea9ea103a191fa</anchor>
      <arglist>(WindowState lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator^</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gad7cefe697a8d5827d435d67fd972453d</anchor>
      <arglist>(WindowState lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator~</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga1c412580d87ff685a76a7b601358253f</anchor>
      <arglist>(WindowState flags)</arglist>
    </member>
    <member kind="function">
      <type>WindowState &amp;</type>
      <name>operator|=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gab0f88bc7fed20644c4a00c96584da6d4</anchor>
      <arglist>(WindowState &amp;lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState &amp;</type>
      <name>operator&amp;=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga51e6c80941660525ae7b153ddd0b742d</anchor>
      <arglist>(WindowState &amp;lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState &amp;</type>
      <name>operator^=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga9b9153faf05f953c09434e31f75dd7e5</anchor>
      <arglist>(WindowState &amp;lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator|</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gac0919c301a5d7895b08ee7421e85726e</anchor>
      <arglist>(WindowActions lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator&amp;</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga105fac92a76fd5f06797ae5f18883b5c</anchor>
      <arglist>(WindowActions lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator^</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gaf2b0380f42bb90c44f369c18acaf180d</anchor>
      <arglist>(WindowActions lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator~</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gab4d93dc376f4facef194af49420636a3</anchor>
      <arglist>(WindowActions flags)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions &amp;</type>
      <name>operator|=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gaca7a811fbfbccd17d3310c82f745e324</anchor>
      <arglist>(WindowActions &amp;lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions &amp;</type>
      <name>operator&amp;=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gac970444847ed1ec9e1da8b81ce6bda0c</anchor>
      <arglist>(WindowActions &amp;lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions &amp;</type>
      <name>operator^=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga566856f713f6a18089a2c46d64f2479e</anchor>
      <arglist>(WindowActions &amp;lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator|</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gad902239da470d5147f52ec22eea535bd</anchor>
      <arglist>(WindowMoveResizeMask lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator&amp;</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga52945cc804facca81f67b1c776c1a72e</anchor>
      <arglist>(WindowMoveResizeMask lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator^</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gad62d7a441b3bff53e0bd650a3b90cf5b</anchor>
      <arglist>(WindowMoveResizeMask lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator~</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gaf1cf3571ef824c38f7cabd10b66eb575</anchor>
      <arglist>(WindowMoveResizeMask flags)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask &amp;</type>
      <name>operator|=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga4ceb354b24cbe550883353270ece3bbd</anchor>
      <arglist>(WindowMoveResizeMask &amp;lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask &amp;</type>
      <name>operator&amp;=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga279f7463307562a03f5b5cce5b00d967</anchor>
      <arglist>(WindowMoveResizeMask &amp;lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask &amp;</type>
      <name>operator^=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga5226568bacc1f61a1ab7cd29d98ec30e</anchor>
      <arglist>(WindowMoveResizeMask &amp;lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
  </compound>
  <compound kind="namespace">
    <name>Glib</name>
    <filename>a00019.html</filename>
  </compound>
  <compound kind="namespace">
    <name>Wnck</name>
    <filename>a00020.html</filename>
    <class kind="class">Wnck::ActionMenu</class>
    <class kind="class">Wnck::Application</class>
    <class kind="class">Wnck::ClassGroup</class>
    <class kind="class">Wnck::Pager</class>
    <class kind="class">Wnck::Screen</class>
    <class kind="class">Wnck::Selector</class>
    <class kind="class">Wnck::Tasklist</class>
    <class kind="class">Wnck::Window</class>
    <class kind="class">Wnck::Workspace</class>
    <member kind="enumeration">
      <type></type>
      <name>PagerDisplayMode</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga62025e8ad5baa49eb952912ca4c1a4bf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PAGER_DISPLAY_NAME</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga62025e8ad5baa49eb952912ca4c1a4bfae7d1779200567f51215a5f6e89430c78</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>PAGER_DISPLAY_CONTENT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga62025e8ad5baa49eb952912ca4c1a4bfaa1301311f7be5541137d611fc41cd5a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowType</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gafc09fd14bddb47633ce0f0c873c03a5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_NORMAL</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa7473ddd8001f6398118b9ad45f858637</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_DESKTOP</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aadcc603879bfdae2bdb7a9708376877eb</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_DOCK</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa667582eaa7887513eb1f5a05b9ae6484</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_DIALOG</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa1a43618225a733c11533e60f49061ac3</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_TOOLBAR</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aab81cd0d9b4a5058f373080161e81d399</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_MENU</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aaffdf6568ca625adaafaf243425e02147</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_UTILITY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa96efe27eec0befaec5bacfdc74164ce4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_SPLASHSCREEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggafc09fd14bddb47633ce0f0c873c03a5aa603924300cb68522bdd9ff92a9ad7fc1</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowState</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga22305f517749f485e0172d74d35ab9f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_MINIMIZED</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a5e3643b2e424f3e6d9c4cacec13893e8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_MAXIMIZED_HORIZONTALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ac39344f28ea43e719e095dcf55781018</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_MAXIMIZED_VERTICALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a3fba6fc435098c81784068658b5ec933</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_SHADED</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a4ff7aee054d5c745d4c6f39c31d7b8a8</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_SKIP_PAGER</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ab11ebc50004373097b110c747a98d6a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_SKIP_TASKLIST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a7ce7becde85ed316c4382da5d50438ad</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_STICKY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a365dee25cf9259f28b0cbb90fd2dda4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_HIDDEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a0efe2534808d0f7360981c8caf2db03b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_FULLSCREEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0af45ef5b2cbc076b80ecc5562cf01fb0c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_DEMANDS_ATTENTION</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0af8cd40e582a8dfd50d95486d10d0cc5b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_URGENT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0a00342aafcf568af56f1179045e4c57ef</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_ABOVE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ae5208f34e3e8da19f23f3e8ed5d7daaa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_STATE_BELOW</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga22305f517749f485e0172d74d35ab9f0ae094e96615a1d2f648b8dbb375ca2c16</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowActions</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga4554e5bb7dcbe177c55d45c106e5ae3f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MOVE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa354275bfada4b85f73e3cdeb209102bd</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_RESIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fac0602212934a0f175881eac5130884d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_SHADE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa3c220b6d5b93188471f736c8f2879daf</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_STICK</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fae2c11c98576901ccbd9d01d913530a7f</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MAXIMIZE_HORIZONTALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa896ab891a6d22ad98ff42c178ee6f67d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MAXIMIZE_VERTICALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fad870b636e87aea30194797fbd39ade3e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_CHANGE_WORKSPACE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fad54c6ba6be26c3ee09fbce56a88135db</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_CLOSE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa1a39b09118e25084fc1cf5c7905acb8a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMAXIMIZE_HORIZONTALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3facff11c69e23432e0b3fc849eafb4c0f2</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMAXIMIZE_VERTICALLY</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fae4e2090d7ef260ad50fb3d562f1d5735</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNSHADE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faed6c72022132c6912fa110f354d2a3a5</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNSTICK</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa38a6ec6f30e16a78a9562348ade947b0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MINIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faae09d8b9a147219664a0506b98f30cc0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMINIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faa246a84b101a90eef672fd9ba6654a2d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_MAXIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa3b2142dd6f0bb4fe41d2bb4ddeda5ed7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_UNMAXIMIZE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faf119a507e8da0a135542c8725d5b2e8c</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_FULLSCREEN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fa3badbbfb5a4ce4c03b3569312baf866e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_ABOVE</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3faed40a6f35230a45ee4564eac7b05d051</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_ACTION_BELOW</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga4554e5bb7dcbe177c55d45c106e5ae3fad38e2327f985f43271b3def9c755bb12</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowGravity</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga1e0e1c715ff933cda7ad971d905fca7a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_CURRENT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa2c32445e0fab2887df4379383ff8ef8d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_NORTHWEST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa8b8262cc0f0c74aacf41b8204263db26</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_NORTH</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa3450459c7598dd6b6809d06849c33b4b</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_NORTHEAST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa43cd181291a7e83c19ddbbfd0d99d74a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_WEST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aaa51fac9afcd31a62d1af1c3d1af804c6</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_CENTER</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa5841b107054c9a0fd914e22f9d3d1ff7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_EAST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa3d8077f4ed7b6a5e34e49bfbf426042a</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_SOUTHWEST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa27b1b3322463590f734efd786c755239</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_SOUTH</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aa55fb1c778605892f6a4261e7af5dfbe4</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_SOUTHEAST</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aaccccd8da3cd9e7d4f0fc90d9eb90cba9</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_GRAVITY_STATIC</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gga1e0e1c715ff933cda7ad971d905fca7aad0682221d70034a2bcbc8e49cba4f412</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>WindowMoveResizeMask</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gab6325f24c6a98ac725413e19747897e0</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_X</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0a4bd34c04608c0162bbd569010807cca7</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_Y</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0a9c5ee5e8617f53613b9abc4aa549d9aa</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_WIDTH</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0ae347219e8602dc92e40668a88366929e</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>WINDOW_CHANGE_HEIGHT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggab6325f24c6a98ac725413e19747897e0a4d6b7e6fae889f26240536d0c4f4682d</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumeration">
      <type></type>
      <name>MotionDirection</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gabf9331c1c4adf47bb2541f987c060934</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_UP</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934ac2d3296d40074d4787e7af10706ea830</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_DOWN</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934a7f6600e4d4189fb51b41e4de14af1259</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_LEFT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934a56a0ad356bc614ec646768ed7dbbb021</anchor>
      <arglist></arglist>
    </member>
    <member kind="enumvalue">
      <type>@</type>
      <name>MOTION_RIGHT</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ggabf9331c1c4adf47bb2541f987c060934a1a5cfc200889357d5f51b1d16e38ddf9</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator|</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gac18aa71cc6301d547df48651d2282002</anchor>
      <arglist>(WindowState lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator&amp;</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga30f8fbe65446e9e73cea9ea103a191fa</anchor>
      <arglist>(WindowState lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator^</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gad7cefe697a8d5827d435d67fd972453d</anchor>
      <arglist>(WindowState lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>operator~</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga1c412580d87ff685a76a7b601358253f</anchor>
      <arglist>(WindowState flags)</arglist>
    </member>
    <member kind="function">
      <type>WindowState &amp;</type>
      <name>operator|=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gab0f88bc7fed20644c4a00c96584da6d4</anchor>
      <arglist>(WindowState &amp;lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState &amp;</type>
      <name>operator&amp;=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga51e6c80941660525ae7b153ddd0b742d</anchor>
      <arglist>(WindowState &amp;lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowState &amp;</type>
      <name>operator^=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga9b9153faf05f953c09434e31f75dd7e5</anchor>
      <arglist>(WindowState &amp;lhs, WindowState rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator|</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gac0919c301a5d7895b08ee7421e85726e</anchor>
      <arglist>(WindowActions lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator&amp;</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga105fac92a76fd5f06797ae5f18883b5c</anchor>
      <arglist>(WindowActions lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator^</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gaf2b0380f42bb90c44f369c18acaf180d</anchor>
      <arglist>(WindowActions lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>operator~</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gab4d93dc376f4facef194af49420636a3</anchor>
      <arglist>(WindowActions flags)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions &amp;</type>
      <name>operator|=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gaca7a811fbfbccd17d3310c82f745e324</anchor>
      <arglist>(WindowActions &amp;lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions &amp;</type>
      <name>operator&amp;=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gac970444847ed1ec9e1da8b81ce6bda0c</anchor>
      <arglist>(WindowActions &amp;lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowActions &amp;</type>
      <name>operator^=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga566856f713f6a18089a2c46d64f2479e</anchor>
      <arglist>(WindowActions &amp;lhs, WindowActions rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator|</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gad902239da470d5147f52ec22eea535bd</anchor>
      <arglist>(WindowMoveResizeMask lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator&amp;</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga52945cc804facca81f67b1c776c1a72e</anchor>
      <arglist>(WindowMoveResizeMask lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator^</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gad62d7a441b3bff53e0bd650a3b90cf5b</anchor>
      <arglist>(WindowMoveResizeMask lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask</type>
      <name>operator~</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>gaf1cf3571ef824c38f7cabd10b66eb575</anchor>
      <arglist>(WindowMoveResizeMask flags)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask &amp;</type>
      <name>operator|=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga4ceb354b24cbe550883353270ece3bbd</anchor>
      <arglist>(WindowMoveResizeMask &amp;lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask &amp;</type>
      <name>operator&amp;=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga279f7463307562a03f5b5cce5b00d967</anchor>
      <arglist>(WindowMoveResizeMask &amp;lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
    <member kind="function">
      <type>WindowMoveResizeMask &amp;</type>
      <name>operator^=</name>
      <anchorfile>a00021.html</anchorfile>
      <anchor>ga5226568bacc1f61a1ab7cd29d98ec30e</anchor>
      <arglist>(WindowMoveResizeMask &amp;lhs, WindowMoveResizeMask rhs)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::ActionMenu</name>
    <filename>a00001.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~ActionMenu</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>ac715e0640fef957c9dc9e8bb2836b18b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckActionMenu *</type>
      <name>gobj</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>a4dcea0421545c9e72bf1561b63b20124</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckActionMenu *</type>
      <name>gobj</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>a119b8b6124d821f9d6f3820aa2fae605</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>ActionMenu</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>a72f82bf4d95b2f09a376028dd45f9ffd</anchor>
      <arglist>(const Window *window)</arglist>
    </member>
    <member kind="function">
      <type>Wnck::ActionMenu *</type>
      <name>wrap</name>
      <anchorfile>a00001.html</anchorfile>
      <anchor>ae911c2a07eaa8733613ebba9cccdf4de</anchor>
      <arglist>(WnckActionMenu *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::Application</name>
    <filename>a00002.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Application</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a25fc5f5781110ca7da8980fb3b4f4724</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckApplication *</type>
      <name>gobj</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a7225a2a99e06917329fd41895d573793</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckApplication *</type>
      <name>gobj</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a8256e44deaa9f5d40f11c9cbd735201c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>gulong</type>
      <name>get_xid</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a1065ba119bc6da26fad069fd02c8f29c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_name</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a05eb81a962e899eff1442106bb35016b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_pid</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a1c8c3963b4e750a461a9ba27312f3aef</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>is_icon_fallback</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a330cc38d839c81bc57db1e95a1fd675f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::RefPtr&lt; Gdk::Pixbuf &gt;</type>
      <name>get_icon</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a595da3028c89e93854c31406988ff75d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::RefPtr&lt; Gdk::Pixbuf &gt;</type>
      <name>get_mini_icon</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ab5330f3862bb01dec35fcd57fb02cceb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_startup_id</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a66bb354d567c58e4da65c457de8fe577</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ListHandle&lt; Window * &gt;</type>
      <name>get_windows</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a69d352ff0633e81c05087152a1211e54</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_window_count</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>afcb62709e8fcc16b1e81c421129f54d8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_name_changed</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a0bf868385beb95690d28a34ebcaf49f5</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_icon_changed</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a3a0836fc0b1f3230ba24a8759aeee1ba</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static GType</type>
      <name>get_type</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>aa42c43dbb5e60a3c07f57985174c3b1e</anchor>
      <arglist>() G_GNUC_CONST</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Window *</type>
      <name>get_for_xid</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>ad15b5df028dc2fd77de9f00677ea7cfb</anchor>
      <arglist>(gulong xid)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Application</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a45056108ed25564a83f4793463db29a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_name_changed</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>af1382a659674b41a2ba8eeea089eb99c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_icon_changed</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>aae6998a2cd81bec8311dd3e8ac0086c9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Wnck::Application *</type>
      <name>wrap</name>
      <anchorfile>a00002.html</anchorfile>
      <anchor>a623445fd2453da3c739cfb7ba6228865</anchor>
      <arglist>(WnckApplication *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::ClassGroup</name>
    <filename>a00003.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~ClassGroup</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a9542349992da62382afb86cce5e54835</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckClassGroup *</type>
      <name>gobj</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a4693a3ed1eb7db82b81451b8fe1fdbd4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckClassGroup *</type>
      <name>gobj</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a0a495ed63c0cbb576b8f8e0c71bde6e3</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_res_class</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>abddb9926accec430c3883180ce05fedf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_name</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>aa9582bdfbfc6feb488bb8510a51782a5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::RefPtr&lt; Gdk::Pixbuf &gt;</type>
      <name>get_icon</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a0889a38c6d6e10eca954a69eac3ee2e6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::RefPtr&lt; Gdk::Pixbuf &gt;</type>
      <name>get_mini_icon</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>aa857e940742729e752aa7cbeb8b76657</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static GType</type>
      <name>get_type</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a9d1a59731e23714498054f301072f804</anchor>
      <arglist>() G_GNUC_CONST</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static ClassGroup *</type>
      <name>get</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a1d6ee5749bc1f3a0d3e530e99fcbd3f6</anchor>
      <arglist>(const std::string &amp;res_class)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>ClassGroup</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>a935549a31908d4cc724801fc1ef66266</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Wnck::ClassGroup *</type>
      <name>wrap</name>
      <anchorfile>a00003.html</anchorfile>
      <anchor>af2f197513a299d885462dffecc1a53a7</anchor>
      <arglist>(WnckClassGroup *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::Pager</name>
    <filename>a00004.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Pager</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a850e0dade01f62c0c31f2abf4796b892</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckPager *</type>
      <name>gobj</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>acd83a3556b6ff37fc0bd0dd124262821</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckPager *</type>
      <name>gobj</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a0edb5a26df58452f519fcc7f68345396</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Pager</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a545e215280f4f2eef2d7ac4c709b12ab</anchor>
      <arglist>(const Screen *screen)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>set_orientation</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a935201c4f2445524fee4db744b59fd9f</anchor>
      <arglist>(Gtk::Orientation orientation)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>set_row_count</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a692147eaf1dcfb0d3fb28edc42176007</anchor>
      <arglist>(int count)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_display_mode</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>ad46730e781c12d3a097afe50c7f0c68b</anchor>
      <arglist>(PagerDisplayMode mode)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_show_all</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>aa2c933538d9792a316dfd30b7d906b01</anchor>
      <arglist>(bool show_all)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_shadow_type</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>af5235e373ed963b145fd1c57d2cea178</anchor>
      <arglist>(Gtk::ShadowType shadow_type)</arglist>
    </member>
    <member kind="function">
      <type>Wnck::Pager *</type>
      <name>wrap</name>
      <anchorfile>a00004.html</anchorfile>
      <anchor>a9d2bf0479ccd04dfc1327ee2268b9932</anchor>
      <arglist>(WnckPager *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::Screen</name>
    <filename>a00005.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Screen</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>abdf5d4806335ff040540c920d2d8919b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckScreen *</type>
      <name>gobj</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a271ad912bfba0ae875552f6bc52d7953</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckScreen *</type>
      <name>gobj</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ab19ec979e69d8e321eabf7e20ff501a6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_number</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>adabaa1d9343e7fcc806b24e71eb19b6c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_width</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a36d31a34dd149a3e0afc8566070360dc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_height</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ab97c7d8036d979f169ce9960506fe7c0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>force_update</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aa472e64aeeb91cc2ff6e25197ce59dfd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_window_manager_name</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ad0f03260d837368468b2aa007bdb4f22</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>net_wm_supports</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a81318560d318534dc0f733ffaa38867a</anchor>
      <arglist>(const std::string &amp;atom)</arglist>
    </member>
    <member kind="function">
      <type>Window *</type>
      <name>get_active_window</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a42487f8dbe7c4988e8df47617be2eea9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Window *</type>
      <name>get_previously_active_window</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a13b0de67d2179b5048325eb9875fbe09</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ListHandle&lt; Window * &gt;</type>
      <name>get_windows</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a96a7aeb5274e313529d69fdac9ec26a9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::ListHandle&lt; Window * &gt;</type>
      <name>get_windows_stacked</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aee806ae08010e59d7d0643b41e8cd781</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Workspace *</type>
      <name>get_active_workspace</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a0d77d4e0efb7ef84ce75bc36ff80488d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Workspace *</type>
      <name>get_workspace</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a99a7102cb5e7e79f4dad463e04df610e</anchor>
      <arglist>(int workspace) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_workspace_count</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ac701ffc190e813cba18dffbde1eac763</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_workspace_count</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a826af44488efe8972bc976e4de758069</anchor>
      <arglist>(int count) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>move_viewport</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a9aa8b05a7259055b1b1d5481b1a7458c</anchor>
      <arglist>(int x, int y) const </arglist>
    </member>
    <member kind="function">
      <type>gulong</type>
      <name>get_background_pixmap_xid</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ad49aee6ebfc3c26cc79104e9ef796e65</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_showing_desktop</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a579e7a7b392585cb76e20ae51d3c7540</anchor>
      <arglist>(bool show) const </arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Window * &gt;</type>
      <name>signal_active_window_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a050c6260a72885ae5824f4f26ebab10c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Workspace * &gt;</type>
      <name>signal_active_workspace_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a5737c1f28ef1e59d19955dcd551cc204</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Application * &gt;</type>
      <name>signal_application_closed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>adfec906d936d771b5f373bec3613881a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Application * &gt;</type>
      <name>signal_application_opened</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a806a64b68a7f7fb451ab042fa3458214</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_backgroupnd_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a14fc6f6aa8995ec32bef64cacc203fb9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, ClassGroup * &gt;</type>
      <name>signal_class_group_closed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>abaeee19e421422b2c3228e62ae2af9ac</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, ClassGroup * &gt;</type>
      <name>signal_class_group_opened</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a46456cb01fc318b83cd1f79b1e62a7f7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_showing_desktop_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ab92598163412d45a1068752e2ec3d934</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_viewports_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a1a5cfc9861e02ccf5cccaf7cf3719bf9</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Window * &gt;</type>
      <name>signal_window_opened</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a7a5dfd2aea7b2dbc60f11f9c2e16326c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Window * &gt;</type>
      <name>signal_window_closed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aaae7446117483fd9c496c19389bf6c9e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_window_manager_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aa90d7b764f0abadde20eea6c193bdd5f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_window_stacking_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a095ccd7e05b4f7e4ad72a4354bcabf80</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Workspace * &gt;</type>
      <name>signal_workspace_created</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aa155cb1b06cdd4d32f239fce9c50c70e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy1&lt; void, Workspace * &gt;</type>
      <name>signal_workspace_destroyed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a31a11a83ec15f1b9f5d6017ee1bcde96</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static GType</type>
      <name>get_type</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a3a0942b174076c3e55107b4a30b3a039</anchor>
      <arglist>() G_GNUC_CONST</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Screen *</type>
      <name>get_default</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a63c37040bc049f70a0ef51526716e42e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Screen *</type>
      <name>get</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ac6a55ac36d6c177be7e5e4b589dce743</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Screen *</type>
      <name>get_for_root</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a4dbec29650911d7906532d9ff6f09918</anchor>
      <arglist>(gulong root_id)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_active_window_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a620ebfd30f344756270f14e33aef58cd</anchor>
      <arglist>(Window *previously_active_window)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_active_workspace_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a1e262fec631935cfa28fc7ba2a160482</anchor>
      <arglist>(Workspace *previously_active_workspace)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_application_closed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aa74e3e1b095350bc8ee43343db22d372</anchor>
      <arglist>(Application *app)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_application_opened</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aa8f6b174de08bf615fb5581cdfc78943</anchor>
      <arglist>(Application *app)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_backgroupnd_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a277578d2154308ba04662068e8fffc0a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_class_group_closed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>aa790f3f4655a22fc8c607ffaeee459f4</anchor>
      <arglist>(ClassGroup *app)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_class_group_opened</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a520e7fc3ab9776ae6dfe23a0135c62d7</anchor>
      <arglist>(ClassGroup *app)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_showing_desktop_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a55d565191a9828ec0c9d4f0f9369589f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_viewports_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ac7e5bc7d94dfa262a4dfad07fce3f219</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_window_opened</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a15e061c94f7a3fc265e55efaa3846037</anchor>
      <arglist>(Window *window)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_window_closed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a2e9de91f25613f3069a98be753d20fdd</anchor>
      <arglist>(Window *window)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_window_manager_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ab62be54675af68bd76205074ba81d5a7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_window_stacking_changed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>ac3502ae053666403ea44321ac60a4e71</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_workspace_created</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>af0432b62efa60a7231fbf98414d3093a</anchor>
      <arglist>(Workspace *workspace)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_workspace_destroyed</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a391123e6ef09a77fd21216562c4c61b7</anchor>
      <arglist>(Workspace *workspace)</arglist>
    </member>
    <member kind="function">
      <type>Wnck::Screen *</type>
      <name>wrap</name>
      <anchorfile>a00005.html</anchorfile>
      <anchor>a3c7632fa27faf23827db878eb483e51e</anchor>
      <arglist>(WnckScreen *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::Selector</name>
    <filename>a00006.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Selector</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ad2dc34441251577c238e9e49b2407a76</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckSelector *</type>
      <name>gobj</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a791060047cfdd16edba4b8c82fcb01bd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckSelector *</type>
      <name>gobj</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>ad84edcc97cda8adccea2443b62a04fbe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>Selector</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a592191f7a0d7281246eeb2076094f00e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Wnck::Selector *</type>
      <name>wrap</name>
      <anchorfile>a00006.html</anchorfile>
      <anchor>a79b26058841a1d8a928ce9d69ba422a8</anchor>
      <arglist>(WnckSelector *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::Tasklist</name>
    <filename>a00007.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Tasklist</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>adb88d99208d7fa749650ae642e676fb3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckTasklist *</type>
      <name>gobj</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>af1681bd85a9398a1cd82f2fdbbab890f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckTasklist *</type>
      <name>gobj</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>ab7f978992eadc679bdaa0f2afe53da6c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Wnck::Tasklist *</type>
      <name>wrap</name>
      <anchorfile>a00007.html</anchorfile>
      <anchor>a867fe130dd9c00fe90a1c3d89381239f</anchor>
      <arglist>(WnckTasklist *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::Window</name>
    <filename>a00008.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Window</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a8b2e5c1fec9cf841456ca3bf6d47c100</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckWindow *</type>
      <name>gobj</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a05e08811b3d1c33960cffd00ac59607e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckWindow *</type>
      <name>gobj</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>aa72a39742fffeca5d610e17e4af235bd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Screen *</type>
      <name>get_screen</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>adf0f3ccebddd9c0197077ee2f45a2071</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>has_name</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a071489cc95de0153e4baa0e811bd0453</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_name</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ae9029480ba459194c9ffedeb521da3c1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>has_icon_name</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>af05340051b5de767f77586f5059f3eaf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_icon_name</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a2a6f14148d79eef43732b4d8e7e6beb2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_icon_fallback</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ad4fc25e728c6c1dc7f7ce9ebafe5e228</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::RefPtr&lt; Gdk::Pixbuf &gt;</type>
      <name>get_icon</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a5dc3f93aefa555fa4ee1afad60e59a9b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Glib::RefPtr&lt; Gdk::Pixbuf &gt;</type>
      <name>get_mini_icon</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>afadbc2b8666175b6ff43f07ab5dc70e6</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Application *</type>
      <name>get_application</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ad248cd130425f72349c85b28da68ebd7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Window *</type>
      <name>get_transient</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a9c0fafe15f6722ab6f7ff278394ea6bc</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>gulong</type>
      <name>get_group_leader</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a2f65b564a4032bc7c422df957229eb34</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>gulong</type>
      <name>get_xid</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a5410a1b6c5bdea3bef7095e973342450</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>ClassGroup *</type>
      <name>get_class_group</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac611523106d4d2909624473e5b2e8a89</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_session_id</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ab2435e82d2b9516c9f98fd2726aa3700</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_session_id_utf8</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a9cb0e93e0612ea34ded9bb609529efcf</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_pid</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ab3f112ea35a8425b9fc6b86ade2ce184</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_sort_order</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a4700ad09d0b8f63780061f2065c0768e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_sort_order</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a86efcff9ee0f68fd134c6c7e33ee62d2</anchor>
      <arglist>(int order)</arglist>
    </member>
    <member kind="function">
      <type>WindowType</type>
      <name>get_window_type</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a27e4f2167e6a0293b42bc0a9df9e12a2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_window_type</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a5ce00bf4ad60fd309afcd8297223540b</anchor>
      <arglist>(WindowType order)</arglist>
    </member>
    <member kind="function">
      <type>WindowState</type>
      <name>get_state</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a7ff524db461f3de8401437fd72282e02</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_minimized</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a5781b6db03601b0e430372454971cd70</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_maximized_horizontally</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a5d17f8743607239de84e7444592e63aa</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_maximized_vertically</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3edc20146f9c2c612cba2de1680e7206</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_maximized</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ae699ffe56a04f961017bb2e2df1bbaa9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_shaded</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a923012d220a9be5721efe306f47befc0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_pinned</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a84ddba380366ca5339fd60002ee70e0b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_sticky</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a8548e4ca1d360374841b5e8f01138319</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_above</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a63c04a1956264450a60bdc87a3d8f9ae</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_below</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a45f78334f95db8a55e487fd73085060c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_skip_pager</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a1e177490bce1941036ae99b8a66d426c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_skip_tasklist</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ab2055246e84f0288463f8a08c8445c6a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_fullscreen</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ad125061c1e44ff7b6a7076a53d910577</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_needs_attention</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>afe8ba6ea8f22d6afaea768beedbc3ada</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_needs_attention_or_transient</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a92322ce5a2764b239a8e8b9e42d8a03f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>WindowActions</type>
      <name>get_actions</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac9fc36950a88553687e66d4aea6bf21d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>minimize</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a86ecc6bbca8778157fa6a1c1648507eb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unminimize</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>aa28f1007a555237d989dd1a567f2a3f1</anchor>
      <arglist>(guint32 timestamp)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>maximize</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a8140dae74b2c7f22df1f74e1abb149be</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unmaximize</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a54ffa58d8ba5de81c4f4ffaf0cc2ecb1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>maximize_vertically</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a77827fff983b995197435699f480277e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unmaximize_vertically</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ad8fc2ba654cb272c540eca14f34ac028</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>maximize_horizontally</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3cea4a939d784b8cd0ae27e176b6034c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unmaximize_horizontally</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>aae87b5f3f3ff5bf4b750ed77cf6903d1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>shade</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>aeefd07f06f09ace00a967b0fdf141921</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unshade</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a4257a685553317b9522c26eeb243274f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>pin</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a1ebb1452dd3158188cf0998ef70aeca0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unpin</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3769fd6ee5cd2bc653f7d82abf0ae1c8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stick</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a33a1350eec45025e1d975955fb9531c7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unstick</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a81fdd8fc5c81a31129f976cfefdc09b8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>make_above</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a0f66d110a093e08182860c0295c8225b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unmake_above</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac997f173dbfa3a9d933c0499f9e4ba16</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>make_below</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a584d5fa23f08c264a59073891a480219</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>unmake_below</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a9a4fa581e646d2e7cc025d09425f2dda</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_skip_pager</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ae5559fab4d3d8cf58fef3cdaf91e9925</anchor>
      <arglist>(bool skip=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_skip_tasklist</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac05120fce31990ce944e8ab0fcaffddd</anchor>
      <arglist>(bool skip=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_fullscreen</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a8a53cebce3e08dfbfc2bde3afa72c00b</anchor>
      <arglist>(bool fullscreen=true)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>close</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a3f92f075450bbba92bb3b7ab197f1983</anchor>
      <arglist>(guint32 timestamp)</arglist>
    </member>
    <member kind="function">
      <type>Workspace *</type>
      <name>get_workspace</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ace643cdf1448f4d843995bf63877dc03</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_on_workspace</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>aeb2e8ac43fbdd9e8d443a58a848eb3ce</anchor>
      <arglist>(const Workspace *workspace) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_visible_on_workspace</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a7c6c2264dd21e89b9b84ff612cd2f2f6</anchor>
      <arglist>(const Workspace *workspace) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>move_to_workspace</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a99ab8a009a2a0249b5c4519d208bc426</anchor>
      <arglist>(const Workspace *workspace)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_in_viewport</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ab8284bec9018756c42c4f5350f8b56ae</anchor>
      <arglist>(const Workspace *workspace) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>activate</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>af48119f587e96888a1d1b85bf546b129</anchor>
      <arglist>(guint32 timestamp)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_active</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a9d6ef3f8fe4d8143bbc466cc6a4e3986</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_most_recently_activated</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>acb1042dd63220544228a4822d6c09938</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>activate_transient</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a63f2e7a78337839ef3d131387453cbb4</anchor>
      <arglist>(guint32 timestamp)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_transient_most_recently_activated</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a188c7a24f55f82253c925b80ef5b7fbd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_icon_geometry</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a0910b3479ea8eb497ddb6acf6fab4f73</anchor>
      <arglist>(int x, int y, int width, int height)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_icon_geometry</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a43a0aec5f326ce202026928f03053425</anchor>
      <arglist>(const Gdk::Rectangle &amp;geom)</arglist>
    </member>
    <member kind="function">
      <type>Gdk::Rectangle</type>
      <name>get_client_geometry</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a27cda951d234477f965f476a24ce90ba</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Gdk::Rectangle</type>
      <name>get_geometry</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>af4520c3a25ad4c4fe4d803d35e63095c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_geometry</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac8a485a73063d2f88b3767d3e49bd924</anchor>
      <arglist>(WindowGravity gravity, WindowMoveResizeMask geometry_mask, int x, int y, int width, int height)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_geometry</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>af222d10a15343f2e06041be683b2896e</anchor>
      <arglist>(WindowGravity gravity, WindowMoveResizeMask geometry_mask, const Gdk::Rectangle &amp;geom)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>keyboard_move</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>aa7138366da44649fd56f0425a998695d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>keyboard_size</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a67fc8c4509894fbe40f769df7760b8d3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy2&lt; void, WindowActions, WindowActions &gt;</type>
      <name>signal_actions_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a680ff080d6fd66e0e6d54a00e7be8459</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_geometry_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a4952d0ff67781c413b76cf4d08f05638</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy2&lt; void, WindowState, WindowState &gt;</type>
      <name>signal_state_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a1bea25e6a7f932aa124f3461dc0e1c0a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_workspace_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac0c0be52107f50d2979235430a575f46</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_icon_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a1424e51ec1282d8d75ed4774a273d0f8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_name_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a238d5339ad6d98d2df44ccc7938a8457</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static GType</type>
      <name>get_type</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac2903b3690c84bba6f39631a4507334e</anchor>
      <arglist>() G_GNUC_CONST</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static Window *</type>
      <name>get_for_xid</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a67b3790e610b3614100877591c29b16e</anchor>
      <arglist>(gulong xid)</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Window</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a6544324aed5f50cd84c53cabb5be3c5d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_actions_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ad7492b7cfcca37b2f15e24b4301139a5</anchor>
      <arglist>(WindowActions changed_mask, WindowActions new_state)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_geometry_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a96917fcf6e3226d4703d02fc9c2377e0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_state_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a31af5588e7e727a64426f65657dc7688</anchor>
      <arglist>(WindowState changed_mask, WindowState new_state)</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_workspace_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac483addd4cec3b30eab4da0e868af601</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_icon_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a092895f18329a1e2cfcaa6877b46b9a2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_name_changed</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>ac8aec06c62fb98080ea200ba7a1c7c6f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Wnck::Window *</type>
      <name>wrap</name>
      <anchorfile>a00008.html</anchorfile>
      <anchor>a27894e7c16a09b3fc2ab0b5b1461244f</anchor>
      <arglist>(WnckWindow *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Wnck::Workspace</name>
    <filename>a00009.html</filename>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~Workspace</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>ace1e24316f985264a3b1cdd2f0163628</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>WnckWorkspace *</type>
      <name>gobj</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a0c241d24b59e6be49eb6a76e190dfb1b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const WnckWorkspace *</type>
      <name>gobj</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a3bd7eb4f5a38108515572e1b711f5e0b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Screen *</type>
      <name>get_screen</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>ab57d08b76461b834e6441a461a47ee56</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_number</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a9be146546f36a12d15cc756fb96edf45</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>std::string</type>
      <name>get_name</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>aaa8fe6689cfaff20e50041a00087de48</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>set_name</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a1de5f14b48b58b01e5a5bfe428bff2a7</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_width</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a2e5e49d89ff781008afc713e022469a8</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_height</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a34c776e86a781ded7a6a6df128e98642</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_viewport_x</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>aec42f9d39cb248ad1f93eaff92c362fb</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_viewport_y</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a8291c215be90e83b42a59bd1fa1fa491</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>is_virtual</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a09875c3001488a39f6c22872a34665e7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_layout_row</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a8df8c4252a90c80ad54532bda22bb8f4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>get_layout_column</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a90c30c3f677103f11041099f0650e1ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>Workspace *</type>
      <name>get_neighbor</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a46044744f7e05ce4c9bd194d2cfbb6d1</anchor>
      <arglist>(MotionDirection direction) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>activate</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>ad9e758b768ef0c09f7e76f5b14c01663</anchor>
      <arglist>(guint32 timestamp)</arglist>
    </member>
    <member kind="function">
      <type>Glib::SignalProxy0&lt; void &gt;</type>
      <name>signal_name_changed</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a55e8f18511fb4ea5ddb5b2bd138fcc59</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static GType</type>
      <name>get_type</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a3d48a8369642eca145e8147a8ec1b08a</anchor>
      <arglist>() G_GNUC_CONST</arglist>
    </member>
    <member kind="function" protection="protected">
      <type></type>
      <name>Workspace</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a9796741da870908a953920f4406aac64</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" protection="protected" virtualness="virtual">
      <type>virtual void</type>
      <name>on_name_changed</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>a4867dc3d262229cdd6a49995fc96b678</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>Wnck::Workspace *</type>
      <name>wrap</name>
      <anchorfile>a00009.html</anchorfile>
      <anchor>af403de938c629de2c4f3c0312445677b</anchor>
      <arglist>(WnckWorkspace *object, bool take_copy=false)</arglist>
    </member>
  </compound>
  <compound kind="dir">
    <name>/home/exec/code/my/checkouts</name>
    <path>/home/exec/code/my/checkouts/</path>
    <filename>dir_ef7ec48b6be1a419d2e2d765e5d052c0.html</filename>
    <dir>/home/exec/code/my/checkouts/libwnckmm</dir>
  </compound>
  <compound kind="dir">
    <name>/home/exec/code/my/checkouts/libwnckmm</name>
    <path>/home/exec/code/my/checkouts/libwnckmm/</path>
    <filename>dir_7e277d1c8964287c2547294aeab69608.html</filename>
    <dir>/home/exec/code/my/checkouts/libwnckmm/wnck</dir>
  </compound>
  <compound kind="dir">
    <name>/home/exec/code/my/checkouts/libwnckmm/wnck</name>
    <path>/home/exec/code/my/checkouts/libwnckmm/wnck/</path>
    <filename>dir_3cb18b12474da0fbae7f60e8a2ccdbfa.html</filename>
    <dir>/home/exec/code/my/checkouts/libwnckmm/wnck/wnckmm</dir>
  </compound>
  <compound kind="dir">
    <name>/home/exec/code/my/checkouts/libwnckmm/wnck/wnckmm</name>
    <path>/home/exec/code/my/checkouts/libwnckmm/wnck/wnckmm/</path>
    <filename>dir_0c234b14f860cd164b3c695e0b5ca907.html</filename>
    <file>action-menu.h</file>
    <file>application.h</file>
    <file>class-group.h</file>
    <file>pager.h</file>
    <file>screen.h</file>
    <file>selector.h</file>
    <file>tasklist.h</file>
    <file>window.h</file>
    <file>workspace.h</file>
  </compound>
</tagfile>
